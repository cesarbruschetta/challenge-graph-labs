import unittest
import os
import json

from flask_testing import TestCase

from graph_webserver import create_app, NODES


class TestGrapthNodesTests(TestCase):
    def setUp(self):

        self.headers = {"Content-Type": "application/json"}

    def create_app(self):
        return create_app("config.TestingConfig")

    def test_grapth_nodes_get_ok(self):

        response = self.client.get("/graph/nodes")
        self.assertStatus(response, 200)

        self.assertEqual(
            ["Ana", "Maria", "Vinicius", "Luiza", "João", "Carlos"],
            response.json["nodes"],
        )

    def test_grapth_nodes_post_ok(self):

        self.assertNotIn("Cesar", NODES.keys())
        response = self.client.post(
            "/graph/nodes",
            data=json.dumps({"name": "Cesar", "knows": ["Ana", "Luiza"]}),
            headers=self.headers,
        )

        self.assertStatus(response, 200)
        self.assertEqual(["Ana", "Luiza"], response.json["node"])

        self.assertIn("Cesar", NODES.keys())

    def test_grapth_nodes_post_with_error_500(self):

        response = self.client.post(
            "/graph/nodes",
            data=json.dumps({"not_name": "Cesar", "knows": ["Ana", "Luiza"]}),
            headers=self.headers,
        )

        self.assertStatus(response, 500)

    def test_grapth_nodes_post_with_error_400(self):

        response = self.client.post("/graph/nodes", data={}, headers=self.headers)
        self.assertStatus(response, 400)


class TestGrapthEdgeTests(TestCase):
    def setUp(self):
        self.headers = {"Content-Type": "application/json"}

    def create_app(self):
        return create_app("config.TestingConfig")

    def test_grapth_edge_get_ok(self):

        response = self.client.get(
            "/graph/edge", data=json.dumps({"name": "Ana"}), headers=self.headers
        )
        self.assertStatus(response, 200)

        self.assertEqual(
            ["Maria", "Vinicius", "João", "Carlos"], response.json["edges"]
        )

    def test_grapth_edge_get_with_400(self):

        response = self.client.get("/graph/edge", data={}, headers=self.headers)
        self.assertStatus(response, 400)


class TestGrapthEdgeParentTests(TestCase):
    def setUp(self):
        self.headers = {"Content-Type": "application/json"}

    def create_app(self):
        return create_app("config.TestingConfig")

    def test_grapth_edge_parent_get_ok_case_1(self):

        response = self.client.get(
            "/graph/edge/parent", data=json.dumps({"name": "Ana"}), headers=self.headers
        )
        self.assertStatus(response, 200)

        self.assertEqual(["Luiza"], response.json["parent"])

    def test_grapth_edge_parent_get_ok_case_2(self):

        response = self.client.get(
            "/graph/edge/parent",
            data=json.dumps({"name": "Maria"}),
            headers=self.headers,
        )
        self.assertStatus(response, 200)

        self.assertEqual(["Carlos", "João"], response.json["parent"])

    def test_grapth_edge_parent_get_with_400(self):

        response = self.client.get("/graph/edge/parent", data={}, headers=self.headers)
        self.assertStatus(response, 400)
