# Setup

## Instalação em Ambiente Local

### Criar o virtualenv

```bash
$ virtualenv -p /usr/bin/python3.7 challenge-graph-labs
```

### Clonar o código fonte da aplicação

```bash
$ cd challenge-graph-labs
$ git clone https://gitlab.com/cesarbruschetta/challenge-graph-labs.git app
```

### Instalar as dependências do python

```bash
$ cd app
$ ../bin/pip install -r requirements.txt
$ ../bin/pip install -e .
```

### Iniciar o processo para desenvolvimento
```bash
$ cd challenge-graph-labs/app
$ ../bin/python graph_webserver.py
```

## Instalação em Ambiente Docker

### Clonar o código fonte da aplicação

```bash
$ git clone https://gitlab.com/cesarbruschetta/challenge-graph-labs.git challenge-graph-labs
```

### Realizando o `build` da imagem docker

```bash
$ cd challenge-graph-labs
$ docker build . -t challenge-graph:latest
```

### Rotando a imagem docker

```bash
$ cd challenge-graph-labs
$ docker run -d -p 8080:8080 --name GraphWebServer challenge-graph:latest
```
