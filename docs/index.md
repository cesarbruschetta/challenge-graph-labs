# Documentação

## Descrição

O problema desse desafio é criar um serviço web que disponibilize o seguinte grafo:

![Exemplo de Grafo gerado](./grafo.png)

Então essa aplicação permite o retorno dos dados para a formação do _Grafo_.
Consulte os demais items da documentação para informações adicionais de instalação, utilização e testes.

## Menu

* [Setup](./setup.md)
* [Testes](./tests.md)
* [API](./api.md)

## Criador

* Cesar Augusto
* cesarabruschetta@gmail.com
* Developer Python
