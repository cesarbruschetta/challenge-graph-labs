## Grafo APIs
API que disponibilize um grafo de relacionamento entre pessoas

### About
| Url                                              | Version |
| ------------------------------------------------ | ------- |
| [http://localhost:8080/](http://localhost:8080/ "API url") | 1.0     |

## Endpoints

## graph/edge
## GET

### Expected Response Types
| Response | Reason             |
| -------- | ------------------ |
| 200      | OK                 |
| 400      | Argumento inválido |

### Parameters
| Name    | In    | Description                           | Required? | Type                                                   |
| ------- | ----- | ------------------------------------- | --------- | ------------------------------------------------------ |
| payload | body  |                                       | true      | [Modelo para Arestas](#modelo para arestas-definition) |

## graph/edge/parent

## GET

### Expected Response Types
| Response | Reason             |
| -------- | ------------------ |
| 200      | OK                 |
| 400      | Argumento inválido |

### Parameters
| Name    | In    | Description                           | Required? | Type                                                   |
| ------- | ----- | ------------------------------------- | --------- | ------------------------------------------------------ |
| payload | body  |                                       | true      | [Modelo para Arestas](#modelo para arestas-definition) |

## graph/nodes

## POST

### Expected Response Types
| Response | Reason             |
| -------- | ------------------ |
| 200      | OK                 |
| 400      | Argumento inválido |
| 500      | Mapping Key Error  |

### Parameters
| Name    | In   | Description | Required? | Type                                           |
| ------- | ---- | ----------- | --------- | ---------------------------------------------- |
| payload | body |             | true      | [Modelo para Nós](#modelo para nós-definition) |

## GET

### Expected Response Types
| Response | Reason |
| -------- | ------ |
| 200      | OK     |

## Definitions

### Modelo para Arestas Definition
| Property | Type   | Format |
| -------- | ------ | ------ |
| name     | string |        |

### Modelo para Nós Definition
| Property | Type   | Format |
| -------- | ------ | ------ |
| name     | string |        |
| knows    | array  |        |


## Additional Resources
[Json Swagger](http://localhost:8080/swagger.json "External Documentation")
