# Teste da aplicação

## Instalar dependencias para os testes

```bash
$ cd /path/to/env/challenge-graph-labs
$ ../bin/pip install -e .
$ ../bin/pip install coverage flask_testing
```

## Executar unittest

```bash
$ cd /path/to/env/challenge-graph-labs
$ ../bin/python test -v
```

## Executar coverage

```bash
$ cd /path/to/env/challenge-graph-labs
$ ../bin/coverage run -m unittest -v
```

## Executar relatorio

```bash
$ cd /path/to/env/challenge-graph-labs
$  coverage report
```
