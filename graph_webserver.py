# coding: utf-8
"""
Module to Webserver
"""
import os
import itertools

from flask import Flask, request
from flask_restplus import Api, Resource, fields

NODES = {
    "Ana": ["Maria", "Vinicius", "João", "Carlos"],
    "Maria": ["Ana", "Vinicius"],
    "Vinicius": ["Ana", "Maria"],
    "Luiza": ["João"],
    "João": ["Ana", "Luiza"],
    "Carlos": ["Ana"],
}


def create_app(settings_ns):
    flask_app = Flask(__name__)
    flask_app.config.from_object(settings_ns)

    app = Api(
        app=flask_app,
        version="1.0",
        title="Grafo APIs",
        description="API que disponibilize um grafo de relacionamento entre pessoas",
    )

    graph_space = app.namespace(
        "graph", description="Representação das relações (Pessoa X Quem ela conhece)"
    )

    node_model = app.model(
        "Modelo para Nós",
        {
            "name": fields.String(
                required=True,
                description="Nome da pessoa",
                help="O nome não pode ser branco.",
            ),
            "knows": fields.List(
                fields.String(
                    required=True,
                    description="Nome da pessoa conhecida",
                    help="O nome não pode ser branco.",
                ),
                description="Lista de pessoas que são conhecidas",
            ),
        },
    )
    edge_model = app.model(
        "Modelo para Arestas",
        {
            "name": fields.String(
                required=True,
                description="Nome da pessoa",
                help="O nome não pode ser branco.",
            )
        },
    )

    @graph_space.route("/nodes")
    class GraphNodeClass(Resource):
        @app.doc(responses={200: "OK"})
        def get(self):
            return {"nodes": list(NODES.keys())}

        @app.doc(
            responses={200: "OK", 400: "Argumento inválido", 500: "Mapping Key Error"}
        )
        @app.expect(node_model)
        def post(self):
            try:
                name = request.json["name"]
                NODES[name] = request.json["knows"]
                return {"node": NODES[name]}
            except KeyError as e:
                graph_space.abort(
                    500,
                    e.__doc__,
                    status="Não foi possível salvar as informações",
                    statusCode="500",
                )
            except Exception as e:
                graph_space.abort(
                    400,
                    e.__doc__,
                    status="Não foi possível salvar as informações",
                    statusCode="400",
                )

    @graph_space.route("/edge")
    class GraphEdgeClass(Resource):
        @app.doc(
            responses={200: "OK", 400: "Argumento inválido"},
            params={"name": "Especifique o nome associado à pessoa"},
        )
        @app.expect(edge_model)
        def get(self):
            try:
                name = request.json["name"]
                return {"edges": NODES[name]}
            except Exception as e:
                graph_space.abort(
                    400,
                    e.__doc__,
                    status="Não foi possível recuperar informações",
                    statusCode="400",
                )

    @graph_space.route("/edge/parent")
    class GraphEdgeParentClass(Resource):
        @app.doc(
            responses={200: "OK", 400: "Argumento inválido"},
            params={"name": "Especifique o nome associado à pessoa"},
        )
        @app.expect(edge_model)
        def get(self):
            try:
                name = request.json["name"]
                black_list = [name] + NODES[name]
                p = set(
                    itertools.chain.from_iterable(NODES[e] for e in NODES[name])
                ) - set(black_list)
                return {"parent": sorted(p)}
            except Exception as e:
                graph_space.abort(
                    400,
                    e.__doc__,
                    status="Não foi possível recuperar informações",
                    statusCode="400",
                )

    return flask_app


def run_app_production():
    return create_app("config.DevelopmentConfig")  # pragma: no cover


def run_app_development():
    return create_app("config.DevelopmentConfig")  # pragma: no cover


if __name__ == "__main__":
    app = run_app_development()
    app.run()
