FROM python:3.7-alpine

COPY ./*.py /app/
COPY README.md /app/
COPY requirements.txt .

RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir waitress \
    && pip install --no-cache-dir -r requirements.txt \
    && pip install --no-cache-dir -e /app/ \
    && rm requirements.txt /app/README.md

WORKDIR /app
USER nobody
EXPOSE 8080

CMD waitress-serve --call 'graph_webserver:run_app_production'