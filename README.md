# Desafio Grafo WebServer

Essa aplicação permite o retorno dos dados para a formação do _Grafo_, como exemplificado abaixo

## O Problema

Seu desafio é criar um serviço web que disponibilize o seguinte grafo:

![Exemplo de Grafo gerado](./docs/grafo.png)

O serviço tem que ter as seguintes funcionalidades:

1. Uma maneira de expor todos os nós do grafo no estado atual
    Ex: No momento inicial essa rota deve retornar: [Ana, Maria, Vinicius, Luiza, João, Carlos]
2. Uma maneira de expor todos as pessoas que uma determinada pessoa conhece (nível 1)
    Ex: Quando enviado o nome Ana, a rota deverá retornar: [Maria, Vinicius, João, Carlos]
3. Uma maneira de expor todos as pessoas que os amigos daquela pessoa conhece, mas ela não conhece (nível 2)
    Ex: Quando enviado o nome Ana, a rota deverá retornar: [Luiza]
4. Uma maneira de cadastrar novas pessoas, informando apenas no cadastro quais são os amigos dela (devolvendo um erro caso algum amigo não exista).

## Requisitos
- Expor a API com algum tipo de documentação exposta em alguma rota, exemplo: Swagger, RAML, API blueprint, etc.
- Criar um DOCKERFILE que exponha de maneira simples esse serviço através de um container, pronto para ser exposto em produção

## Documentação

* [Setup](./docs/setup.md)
* [Testes](./docs/tests.md)
* [API](./docs/api.md)
