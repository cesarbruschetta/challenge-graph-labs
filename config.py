# coding:utf-8
import os


class ProductionConfig(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True


class DevelopmentConfig(ProductionConfig):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(ProductionConfig):
    TESTING = True
