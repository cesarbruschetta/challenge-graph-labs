#!/usr/bin/env python
# coding: utf-8

from setuptools import setup, find_packages

long_description = open("README.md", "r").read()

install_requirements = ["flask", "flask-restplus"]
tests_requirements = ["flask_testing"]


setup(
    name="graph-webserver",
    version=1.0,
    author="Cesar Augusto",
    author_email="cesarabruschetta@gmail.com",
    description="Serviço web que disponibilize um grafo de relacionamento entre pessoas",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="2-clause BSD",
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requirements,
    tests_require=tests_requirements,
    python_requires=">=3.7",
    test_suite="tests",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Other Environment",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
)
